import defaultSettings from '@/settings'

const title = defaultSettings.title || 'cat-manage'

export default function getPageTitle(pageTitle) {
  if (pageTitle) {
    return `${pageTitle} - ${title}`
  }
  return `${title}`
}
