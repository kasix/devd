import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [
      {
        path: 'dashboard',
        name: 'Dashboard',
        component: () => import('@/views/dashboard/index'),
        meta: { title: '首页', icon: 'dashboard' }
      }
    ]
  },
  {
    path: '/user',
    component: Layout,
    children: [
      {
        children: [],
        path: '/wxUser',
        name: 'View',
        component: () => import('@/views/wxuser/index'),
        meta: { title: '查看小程序用户', icon: 'table' }
      }
    ]
  },
  {
    path: '/cat',
    component: Layout,
    children: [
      {
        children: [],
        path: 'cat-add',
        name: 'add',
        component: () => import('@/views/cat/catadd/index'),
        meta: { title: '', icon: 'cat' }
      },
      {
        path: 'cat-edit',
        name: 'edit',
        component: () => import('@/views/cat/catedit/index'),
        meta: { title: '', icon: 'cat' }
      },
      {
        path: 'cat-delete',
        name: 'delete',
        component: () => import('@/views/cat/catedit/index'),
        meta: { title: '', icon: 'cat' }
      },
      {
        path: 'cat-query',
        name: 'query',
        component: () => import('@/views/cat/catedit/index'),
        meta: { title: '', icon: 'cat' }
      }
    ]
  },
  {
    path: '/dic',
    component: Layout,
    children: [
      {
        path: 'dic-add',
        name: 'query',
        component: () => import('@/views/catdatazidian/datazidian.vue'),
        meta: { title: '', icon: 'cat' }
      },
      {
        path: 'dic-edit',
        name: 'edit',
        component: () => import('@/views/catdatazidian/index'),
        meta: { title: '', icon: 'cat' }
      },
      {
        path: 'dic-delete',
        name: 'delete',
        component: () => import('@/views/catdatazidian/index'),
        meta: { title: '', icon: 'cat' }
      },
      {
        path: 'dic-query',
        name: 'query',
        component: () => import('@/views/catdatazidian/index'),
        meta: { title: '', icon: 'cat' }
      }
    ]
  },
  {
    path: '/user',
    component: Layout,
    children: [
      {
        path: 'user-add',
        name: 'add',
        component: () => import('@/views/catuser/index.vue'),
        meta: { title: '', icon: 'cat' }
      },
      {
        path: 'user-edit',
        name: 'edit',
        component: () => import('@/views/catuser/index'),
        meta: { title: '', icon: 'cat' }
      },
      {
        path: 'user-delete',
        name: 'delete',
        component: () => import('@/views/catuser/index'),
        meta: { title: '', icon: 'cat' }
      },
      {
        path: 'user-query',
        name: 'query',
        component: () => import('@/views/catuser/index'),
        meta: { title: '', icon: 'cat' }
      }
    ]
  },
  {
    path: '/log',
    component: Layout,
    children: [
      {
        path: 'log-query',
        name: 'query',
        component: () => import('@/views/catlog/index'),
        meta: { title: '', icon: 'cat' }
      }
    ]
  },

  {
    path: '/menu',
    component: Layout,
    children: [
      {
        path: 'menu-add',
        name: 'query',
        component: () => import('@/views/catmenulist/add.vue'),
        meta: { title: '', icon: 'cat' }
      },
      {
        path: 'menu-edit',
        name: 'edit',
        component: () => import('@/views/catmenulist/index'),
        meta: { title: '', icon: 'cat' }
      },
      {
        path: 'menu-delete',
        name: 'delete',
        component: () => import('@/views/catmenulist/index'),
        meta: { title: '', icon: 'cat' }
      },
      {
        path: 'menu-query',
        name: 'query',
        component: () => import('@/views/catmenulist/index'),
        meta: { title: '', icon: 'cat' }
      }
    ]
  },

  {
    path: '/catmanage',
    component: Layout,
    children: [
      {
        path: 'catmanage-add',
        name: 'add',
        component: () => import('@/views/catwfeedPerson/index.vue'),
        meta: { title: '', icon: 'cat' }
      },
      {
        path: 'catmanage-edit',
        name: 'edit',
        component: () => import('@/views/catwfeedPerson/index'),
        meta: { title: '', icon: 'cat' }
      },
      {
        path: 'catmanage-delete',
        name: 'delete',
        component: () => import('@/views/catwfeedPerson/index'),
        meta: { title: '', icon: 'cat' }
      },
      {
        path: 'catmanage-query',
        name: 'query',
        component: () => import('@/views/catwfeedPerson/index'),
        meta: { title: '', icon: 'cat' }
      }
    ]
  },
  {
    path: '/cathelp',
    component: Layout,
    children: [
      {
        path: 'cathelp-query',
        name: 'query',
        component: () => import('@/views/cathelp/index'),
        meta: { title: '', icon: 'cat' }
      }, {

        path: 'cathelp-edit',
        name: 'edit',
        component: () => import('@/views/cathelp/index'),
        meta: { title: '', icon: 'cat' }
      }, {

        path: 'cathelp-add',
        name: 'add',
        component: () => import('@/views/cathelp/add.vue'),
        meta: { title: '', icon: 'cat' }
      }
    ]
  },
  {
    path: '/menu',
    component: Layout,
    children: [
      {
        children: [],
        path: '/menu-query',
        name: 'query',
        component: () => import('@/views/menu/menuedit/index'),
        meta: { title: '', icon: 'cat' }
      },
      {
        children: [],
        path: '/menu-edit',
        name: 'edti',
        component: () => import('@/views/menu/menuedit/index'),
        meta: { title: '', icon: 'cat' }
      },
      {
        children: [],
        path: '/menu-add',
        name: 'add',
        component: () => import('@/views/menu/menuadd/index'),
        meta: { title: '', icon: 'cat' }
      }
    ]
  },
  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () =>
  new Router({
    // mode: 'history', // require service support
    scrollBehavior: () => ({ y: 0 }),
    routes: constantRoutes
  })

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
