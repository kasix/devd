import request from '@/utils/request'

const pat = '/applyInfo/v1'
// 分页查询申请创建活动的申请信息
export function getList(status, params) {
  return request({
    url: `${pat}/createActivity/${status}`,
    method: 'get',
    params
  })
}

// 审批入驻/活动创建
export function setInspect(params) {
  return request({
    url: `${pat}/inspect`,
    method: 'put',
    params
  })
}
