import request from '@/utils/request'

const pat = '/advert/v1'
// 查看广告列表
export function getList(params) {
  return request({
    url: pat,
    method: 'get',
    params
  })
}

// 查看广告详情
export function getDetail(params) {
  return request({
    url: `${pat}/${params}`,
    method: 'get'
  })
}

// 编辑广告
export function advertEdit(params) {
  return request({
    method: 'put',
    url: `${pat}`,
    data: params
  })
}
// 删除广告
export function advertDel(params) {
  return request({
    url: `${pat}/${params}`,
    method: 'delete'
  })
}
// 启用广告
export function advertStart(params) {
  return request({
    url: `${pat}/${params}`,
    method: 'put'
  })
}

// 添加广告
export function advertAdd(params) {
  return request({
    method: 'post',
    url: `${pat}`,
    data: params
  })
}
// 删除图片
export function imgDel(params) {
  return request({
    url: `/file/v1/${params}`,
    method: 'delete'
  })
}
