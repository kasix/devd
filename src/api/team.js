import request from '@/utils/request'

const pat = '/league/v1'
// 分页查询申请入驻平台的社团申请信息
export function getApplyList(status, params) {
  return request({
    url: `${pat}/joinPlatformInfo/${status}`,
    method: 'get',
    params
  })
}
// 分页查询待审批创建的社团
export function getCreateList(params) {
  return request({
    url: `${pat}/wait`,
    method: 'get',
    params
  })
}
// 审批创建社团
export function setCreateInspect(params) {
  return request({
    url: `${pat}`,
    method: 'put',
    params
  })
}
// 分页查询社团
export function getTeamList(params) {
  return request({
    url: `${pat}`,
    method: 'get',
    params
  })
}
// 冻结社团
export function setFreeze(id, params) {
  return request({
    url: `${pat}/freeze/${id}`,
    method: 'put',
    params
  })
}
