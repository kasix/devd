import request from '@/utils/request'

const pat = '/user/page'
// 查看微信小程序用户列表
export function getList(params) {
  return request({
    url: pat,
    method: 'get',
    params
  })
}
