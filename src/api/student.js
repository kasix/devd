import request from '@/utils/request'

const pat = '/gs/v1'
// 查看学生列表
export function getList(params) {
  return request({
    url: pat,
    method: 'get',
    params
  })
}
// 编辑学生
export function studentEdit(params) {
  return request({
    method: 'put',
    url: `${pat}`,
    data: params
  })
}
// 删除学生
export function studentDel(params) {
  return request({
    url: `${pat}/${params}`,
    method: 'delete'
  })
}
