import request from '@/utils/request'

const pat = '/log'
// 查询日志记录
export function getList(params) {
  return request({
    url: `${pat}`,
    method: 'get',
    params
  })
}
