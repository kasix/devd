import request from '@/utils/request'

// 查看广告列表
export function getadType() {
  return request({
    url: '/type/v1/adType',
    method: 'get'
  })
}
