
import request from '@/utils/request'

// 添加猫列表
export function catadd(params) {
  return request({
    url: '/sys/cat/add',
    method: 'post',
    header:{
        "Content-Type": "multipart/form-data"
      },
   data:params
  })
}
// 添加猫列表
export function dictionaryadd(params) {
  return request({
    url: '/sys/dictionary/add',
    method: 'post',
    data:params
  })
}


//猫管理列表
export function getcatadList(params) {
  return request({
    url: '/sys/cat/page',
    method: 'get',
    params
  })
}
//小程序用户管理列表
export function getcatuserList(params) {
  return request({
    url: '/user/page',
    method: 'get',
    params
  })
}

//字典数据/sys/dictionary/page
export function getdictionaryadList(params) {
  return request({
    url: '/sys/dictionary/page',
    method: 'get',
    params
  })
}

//猫管删除
export function delcat(id) {
  return request({
    url: `/sys/cat/${id}`,
    method: 'delete',

  })
}

//猫管详情
export function getcat(id) {
  return request({
    url: `/sys/cat/${id}`,
    method: 'get',

  })
}

//字典详情
export function getdictionary(id) {
  return request({
    url: `/sys/dictionary/${id}`,
    method: 'get',

  })
}


//字典删除
export function deldictionary(id) {
  return request({
    url: `/sys/dictionary/${id}`,
    method: 'delete',

  })
}



//猫管理列表
export function getlog(params) {
  return request({
    url: '/sys/log/page',
    method: 'get',
    params
  })
}


//小程序用户列表
export function getfeedPerson(params) {
  return request({
    url: '/user/feedPerson',
    method: 'get',
    params
  })
}

//小程序用户编辑


export function getuseredit(params) {
  return request({
    url: '/user/add',
    method: 'post',
    data:params
  })
}


//菜单管理查询 /sys/menu/page
export function getmenuList(params) {
  return request({
    url: '/sys/menu/page',
    method: 'get',
    params
  })
}

// 添加菜单列表
export function menuadd(params) {
  return request({
    url: '/sys/menu/add',
    method: 'post',
    data:params
  })
}

//菜单删除
export function delmenu(id) {
  return request({
    url: `/sys/menu/${id}`,
    method: 'delete',

  })
}

//菜单详情
export function getmenu(id) {
  return request({
    url: `/sys/menu/${id}`,
    method: 'get',

  })
}


//菜单管理查询 /sys/menu/page
export function getfeedPersonlist(params) {
  return request({
    url: '/user/feedPerson',
    method: 'get',
    params
  })
}


//猫救助管理列表
export function getcathelpList(params) {
  return request({
    url: '/sys/help/page',
    method: 'get',
    params
  })
}

// 添加猫救助
export function helpadd(params) {
  return request({
    url: '/sys/help/add',
    method: 'post',
    data:params
  })
}


//菜单删除
export function delhelp(id) {
  return request({
    url: `/sys/help/${id}`,
    method: 'delete',

  })
}

//菜单删除
export function gethelp(id) {
  return request({
    url: `/sys/help/${id}`,
    method: 'get',

  })
}
